import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { OpenTransactionComponent } from './open-transaction/open-transaction.component';
import { TransactionDashboardComponent } from './transaction-dashboard/transaction-dashboard.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'transaction', component: TransactionDashboardComponent },
  { path: 'opentrans', component: OpenTransactionComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
