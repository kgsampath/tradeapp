import {AfterViewInit, Component, ViewChild, OnInit, QueryList} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { min } from 'rxjs';

export interface UserData {
  txtno: string;
  date: string;
  underlying: string;
  asset: string;
  descr: string;
  opened: string;
  qty: string;
  oprice: string;
  ocommission: string;
  closed: string;
  cprice: string;
  ccommission: string;
  gain: string;
  week: string;
  days: string;
  hours: string;
  min: string;
  sec: string;
}


@Component({
  selector: 'app-open-transaction',
  templateUrl: './open-transaction.component.html',
  styleUrls: ['./open-transaction.component.css']
})


export class OpenTransactionComponent implements AfterViewInit  {

  //displayedColumns: string[] = ['Txn No', 'Date','Underlying','Asset Class','Descr','Opened On','Qty','O Price','O Commission','Closed On','C Price','C Commission','Gain','Day of Week','Days','Hours','Minutes','Seconds'];
displayedColumns: string[] = ['txtno','date','underlying','asset','descr','opened','qty','oprice','ocommission','closed','cprice','ccommission','gain','week','days','hours','min','sec'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  constructor() {
    
    //const openData = Array.from({length: 100}, (_, k) => createNewUser(k + 1));
    let openData: UserData[] = [];
     openData =[
      {txtno: '1', date: 'Hydrogen', underlying: '1.0079', asset: 'H', descr: 'description',opened: '1000',qty:'50',oprice: '10000.50', ocommission: '5%',closed: '50',cprice: '5000', ccommission: '3%', gain: '10',  week: '2',  days: '20', hours: '4', min: '2', sec: '7'},
      {txtno: '2', date: 'Hydrogen', underlying: '1.0079', asset: 'H', descr: 'description',opened: '1000',qty:'50',oprice: '10000.50', ocommission: '5%',closed: '50',cprice: '5000', ccommission: '3%', gain: '10',  week: '2',  days: '20', hours: '4', min: '2', sec: '7'},
      {txtno: '3', date: 'Hydrogen', underlying: '1.0079', asset: 'H', descr: 'description',opened: '1000',qty:'50',oprice: '10000.50', ocommission: '5%',closed: '50',cprice: '5000', ccommission: '3%', gain: '10',  week: '2',  days: '20', hours: '4', min: '2', sec: '7'},
      {txtno: '4', date: 'Hydrogen', underlying: '1.0079', asset: 'H', descr: 'description',opened: '1000',qty:'50',oprice: '10000.50', ocommission: '5%',closed: '50',cprice: '5000', ccommission: '3%', gain: '10',  week: '2',  days: '20', hours: '4', min: '2', sec: '7'},
      {txtno: '5', date: 'Hydrogen', underlying: '1.0079', asset: 'H', descr: 'description',opened: '1000',qty:'50',oprice: '10000.50', ocommission: '5%',closed: '50',cprice: '5000', ccommission: '3%', gain: '10',  week: '2',  days: '20', hours: '4', min: '2', sec: '7'},
      {txtno: '6', date: 'Hydrogen', underlying: '1.0079', asset: 'H', descr: 'description',opened: '1000',qty:'50',oprice: '10000.50', ocommission: '5%',closed: '50',cprice: '5000', ccommission: '3%', gain: '10',  week: '2',  days: '20', hours: '4', min: '2', sec: '7'},
      {txtno: '7', date: 'Hydrogen', underlying: '1.0079', asset: 'H', descr: 'description',opened: '1000',qty:'50',oprice: '10000.50', ocommission: '5%',closed: '50',cprice: '5000', ccommission: '3%', gain: '10',  week: '2',  days: '20', hours: '4', min: '2', sec: '7'},
      {txtno: '8', date: 'Hydrogen', underlying: '1.0079', asset: 'H', descr: 'description',opened: '1000',qty:'50',oprice: '10000.50', ocommission: '5%',closed: '50',cprice: '5000', ccommission: '3%', gain: '10',  week: '2',  days: '20', hours: '4', min: '2', sec: '7'},
      
    ];
    
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(openData);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

/** Builds and returns a new User. */
/*function createNewUser(id: number): UserData {
 
  return {
    txtno: txtno,
    date: date,
    underlying: underlying,
    asset: asset,
    descr: descr,
    opened: opened,
    qty: qty,
    oprice: oprice,
    ocommission: ocommission,
    closed: closed,
    cprice: cprice,
    ccommission: ccommission,
    gain: gain,
    week: week,
    hours: hours,
    min: min,
    sec: sec
  };
}*/
